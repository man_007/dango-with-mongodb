from django.db import models

# Create your models here.

class Todo(models.Model):
	title = models.CharField(max_length=120)
	Description = models.TextField()
	date_added = models.DateTimeField()
	date_doneby = models.DateTimeField()

	def __str__(self):
		return self.title
