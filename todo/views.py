from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Todo
from .forms import add_work
# Create your views here.

def home(request):
	context = {}
	context['work'] = Todo.objects.all()
	if request.method=="POST":
		form = add_work(request.POST)
		form.save()
		title = form.cleaned_data.get('title')
		messages.success(request, f'{title} is added in your work list.')
		return redirect('home')
	else:
		form = add_work()


	return render(request, 'home.html', {'data':context, 'form':form})

